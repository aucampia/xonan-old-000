SHELL=bash

python=python3
pip=$(python) -m pip
pylint=$(python) -m pylint
virtualenv=$(python) -m virtualenv
twine=$(python) -m twine

all:

.PHONY: winstall
winstall:
	$(pip) install --user --upgrade $(shell $(python) setup.py --name)

.PHONY: einstall
einstall:
	$(pip) install --user --upgrade --editable .

.PHONY: uninstall
uninstall:
	$(pip) uninstall --yes $(shell $(python) setup.py --name)

.PHONY: test
test:
	$(python) setup.py test

.PHONY: clean
clean: venv-clean
	$(python) setup.py clean
	rm -vrf build/
	rm -vrf docker/build/

.PHONY: distclean
distclean: clean
	find . -name '__pycache__' | xargs --no-run-if-empty -t rm -r
	find . -name '*.pyc' | xargs --no-run-if-empty -t rm
	find . -name '*.egg-info' | xargs --no-run-if-empty -t rm -r
	find . \( -name '.eggs' -o -name '.pytest_cache' \) | xargs --no-run-if-empty -t rm -r

.PHONY: lint
lint:
	#$(pylint) src/*
	find . \( -name build -o -name .eggs -o -name '*.egg-info' -o -name docs -o -name generated -o -name venv \) -prune -o -name '*.py' -type f -print0 | xargs -t -0 $(pylint)

.PHONY: venv
venv:
	$(virtualenv) --no-site-packages ./venv/
	./venv/bin/pip install --upgrade --editable .

.PHONY: venv-clean
venv-clean:
	rm -rf venv

.PHONY: wheel
wheel:
	$(python) setup.py clean --all
	rm -rf dist/ *.egg-info
	$(python) setup.py bdist_wheel
	$(twine) check dist/*

twine_upload=$(twine) upload
twine_upload_args=$${PYPIRC_PATH:+--config-file=$${PYPIRC_PATH}}
twine_upload_with_args=$(twine_upload) $(twine_upload_args)

.PHONY: bdist-upload
bdist-upload: wheel
	$(twine_upload_with_args) dist/*

.PHONY: versioneer
versioneer:
	$(python) versioneer.py setup
	sed -i '/^[^*].*_version.py/d' .gitattributes
	sed -i '/^include .*_version.py/d' MANIFEST.in
	git add .gitattributes MANIFEST.in
