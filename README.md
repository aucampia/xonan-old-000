# xonan

e`x`tentions for [`conan`](https://conan.io/) ...

xonan makes it possile to run binaries from conan packages.

```bash
## Install conan and xadix.xonan
pip3 install --user --upgrade conan xadix.xonan

## add xadix remote
conan remote add -i 0 xadix-conan https://api.bintray.com/conan/xadix/xadix-conan

## Get info about a package
xadix.xonan dump groovy_prebuilt/2.5.6@xadix/default

## run some commands from a package
xadix.xonan exec groovy_prebuilt/2.5.6@xadix/default -- java -version
xadix.xonan exec groovy_prebuilt/2.5.6@xadix/default -- groovysh
xadix.xonan exec 'groovy_prebuilt/[>2.0]@xadix/default' -- java -version
```

## ...

```bash
make uninstall
make einstall

```


## next

 - env (runs shell with environment set)
 - info (just dumps variables)
 - make available globally without needing to run command all the time
  - way to update
 - setup default config

## done

 - published recipes so that command works
 - publish it
 - exec works \o/ ... with ranges ...
 - Added ArgParseNode
 - Moved to seperate repo with scaffolding for proper project management

## References

* https://docs.conan.io/en/latest/
* https://docs.conan.io/en/latest/reference.html
* https://gitlab.com/xadix/scratchpad-2019-000

```bash
pydoc3 conan
```

## trash

```
xadix.xonan exec groovy_prebuilt@xadix -- java -version
```
