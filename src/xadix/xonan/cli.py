#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:

# https://github.com/conan-io/conan/blob/develop/conans/client/conan_api.py
#pylint: disable=line-too-long,pointless-string-statement
#pylint: disable=unused-variable,unused-argument,consider-using-enumerate,protected-access
#pylint: disable=too-many-locals

import logging
import sys
#import argparse
#import fnmatch
import os
#import re
#import subprocess
import json
import datetime
#import inspect
#import pprint

from conans.client.conan_api import Conan
from conans.model.ref import ConanFileReference
from conans.client.graph.printer import print_graph
from conans.client.output import ConanOutput
from conans.client.installer import BinaryInstaller

from . import __version__

from .argparse_tree import ArgParseNode

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))

def do_dump(apn_root, parse_result):
    (api, cache, user_io) = Conan.factory()
    logging.info("(%s)cache = %s", type(cache), cache)
    logging.info("(%s)user_io = %s", type(user_io), user_io)
    refs = parse_result.reference[0]
    ref = ConanFileReference.loads(refs)

    logging.info("cache.package_layout.packages(ref) = %s", cache.package_layout(ref).packages())

    settings = json.loads(parse_result.settings_json) if parse_result.settings_json is not None else None
    options = json.loads(parse_result.options_json) if parse_result.options_json is not None else None

    install_info = api.install_reference(ref, options=options, settings=settings)
    #logging.info("install_info = %s", json.dumps(install_info, sort_keys=True, indent=2, default=json_serial))

    (deps_graph, conanfile) = api.info(refs, options=options, settings=settings)
    # ./conans/client/graph/graph.py
    logging.info("(%s)deps_graph = %s", type(deps_graph), deps_graph)
    output = ConanOutput(sys.stderr, color=True)
    print_graph(deps_graph, output)

    nodes_by_level = deps_graph.by_levels()
    inverse_levels = {n: i for i, level in enumerate(deps_graph.inverse_levels()) for n in level}

    root_level = nodes_by_level.pop()
    root_node = root_level[0]

    for level_index in range(len(nodes_by_level)):
        level = nodes_by_level[level_index]
        for node_index in range(len(level)):
            logging.info("################################################################################")
            node = level[node_index]
            #BinaryInstaller._propagate_info(node, inverse_levels, deps_graph)
            BinaryInstaller._propagate_info(node)
            conanfile = node.conanfile
            logging.info("[l=%s, n=%s]conanfile = %s, node = %s, ref = %s", level_index, node_index, conanfile, node, node.ref)
            if node.ref:
                package_path = os.path.join(cache.package_layout(node.ref).packages(), conanfile.info.package_id())
                logging.info("package_path = %s", package_path)
                BinaryInstaller._call_package_info(conanfile, package_path)
            logging.info("conanfile = %s", conanfile)
            logging.info("conanfile.env = %s", conanfile.env)
            logging.info("conanfile.env.items() = %s", conanfile.env.items())
            logging.info("conanfile.env_info = %s", conanfile.env_info)
            if conanfile.env_info:
                logging.info("conanfile.env_info.vars = %s", conanfile.env_info.vars)

    logging.info("################################################################################")
    #BinaryInstaller._propagate_info(root_node, inverse_levels, deps_graph)
    BinaryInstaller._propagate_info(root_node)
    conanfile = root_node.conanfile
    logging.info("conanfile = %s", conanfile)
    logging.info("conanfile.env = %s", conanfile.env)
    logging.info("conanfile.env.items() = %s", conanfile.env.items())
    logging.info("conanfile.deps_cpp_info = %s", conanfile.deps_cpp_info)
    logging.info("conanfile.deps_env_info = %s", conanfile.deps_env_info)
    logging.info("conanfile.deps_user_info = %s", conanfile.deps_user_info)
    if conanfile.env_info:
        logging.info("conanfile.env_info.vars = %s", conanfile.env_info.vars)
    logging.info("conanfile.deps_env_info.vars = %s", conanfile.deps_env_info.vars)
    logging.info("conanfile.deps_cpp_info.dependencies = %s", conanfile.deps_cpp_info.dependencies)

def do_exec(apn_root, parse_result):
    (api, cache, user_io) = Conan.factory()
    logging.debug("(%s)cache = %s", type(cache), cache)
    logging.debug("(%s)user_io = %s", type(user_io), user_io)
    refs = parse_result.reference[0]
    ref = ConanFileReference.loads(refs)

    logging.debug("cache.package_layout.packages(ref) = %s", cache.package_layout(ref).packages())

    settings = json.loads(parse_result.settings_json) if parse_result.settings_json is not None else None
    options = json.loads(parse_result.options_json) if parse_result.options_json is not None else None

    install_info = api.install_reference(ref, options=options, settings=settings)

    (deps_graph, conanfile) = api.info(refs, options=options, settings=settings)
    logging.debug("(%s)deps_graph = %s", type(deps_graph), deps_graph)
    output = ConanOutput(sys.stderr, color=True)
    print_graph(deps_graph, output)

    nodes_by_level = deps_graph.by_levels()
    inverse_levels = {n: i for i, level in enumerate(deps_graph.inverse_levels()) for n in level}

    root_level = nodes_by_level.pop()
    root_node = root_level[0]

    for level_index in range(len(nodes_by_level)):
        level = nodes_by_level[level_index]
        for node_index in range(len(level)):
            node = level[node_index]
            BinaryInstaller._propagate_info(node)
            conanfile = node.conanfile
            if node.ref:
                package_path = os.path.join(cache.package_layout(node.ref).packages(), conanfile.info.package_id())
                BinaryInstaller._call_package_info(conanfile, package_path)

    BinaryInstaller._propagate_info(root_node)
    conanfile = root_node.conanfile
    logging.debug("conanfile.env = %s", conanfile.env)
    new_env = dict(os.environ)
    append_paths = {'PATH', 'MANPATH', 'LD_LIBRARY_PATH'}
    for (key, value) in conanfile.env.items():
        logging.debug("key = %s, value = %s", key, value)
        if isinstance(value, list):
            string_value = os.pathsep.join(value)
            if (key in append_paths) and (key in os.environ):
                string_value = os.pathsep.join([string_value, os.environ[key]])
        else:
            string_value = value
        logging.debug("key = %s, string_value = %s", key, string_value)
        new_env[key] = string_value
    exec_file = parse_result.xargs[0]
    logging.debug("os.execvpe(%s, %s, ...)", exec_file, parse_result.xargs)
    os.execvpe(exec_file, parse_result.xargs, new_env)

def main():
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr, format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")

    apn_root = ArgParseNode(options={"add_help": True})
    apn_root.parser.add_argument("-v", "--verbose", action="count", dest="verbosity", help="increase verbosity level")
    apn_root.parser.add_argument("--version", action="version", version="{:s}".format(__version__))

    apn_tmp = apn_dump = apn_root.get("dump")

    apn_tmp.parser.add_argument("--settings-json", "--sj", "-S", action="store", type=str, help="")
    apn_tmp.parser.add_argument("--options-json", "--oj", "-O", action="store", type=str, help="")
    apn_tmp.parser.add_argument("reference", action="store", nargs=1, help="conan reference")

    apn_tmp = apn_exec = apn_root.get("exec")

    apn_tmp.parser.add_argument("--settings-json", "--sj", "-S", action="store", type=str, help="")
    apn_tmp.parser.add_argument("--options-json", "--oj", "-O", action="store", type=str, help="")
    apn_tmp.parser.add_argument("reference", action="store", nargs=1, help="conan reference")
    apn_tmp.parser.add_argument("xargs", action="store", nargs='+', help="extra arguments")

    parse_result = apn_root.parser.parse_args(args=sys.argv[1:])
    selected_node = apn_root.get_selected_node(parse_result)

    if parse_result.verbosity is not None:
        root_logger = logging.getLogger("")
        new_level = (root_logger.getEffectiveLevel() - (min(1, parse_result.verbosity))*10 - min(max(0, parse_result.verbosity - 1), 9)*1)
        root_logger.setLevel(new_level)
        root_logger.propagate = True

    logging.info("sys.argv = %s, parse_result = %s, logging.level = %s", sys.argv, parse_result, logging.getLogger("").getEffectiveLevel())


    if selected_node is apn_dump:
        do_dump(apn_root, parse_result)
    elif selected_node is apn_exec:
        do_exec(apn_root, parse_result)

if __name__ == "__main__":
    main()

    ''''
    level = levels[len(levels) - 2]
    logging.info("level = %s", level)
    node = levels[len(levels) - 2][0]
    logging.info("node = %s", node)
    conanfile = levels[len(levels) - 2][0].conanfile
    logging.info("conanfile = %s", conanfile)
    logging.info("(%s)conanfile = %s", type(conanfile), conanfile)
    #logging.info("dir(conanfile) = %s", dir(conanfile))
    #logging.info("inspect.getmembers(conanfile) = %s", inspect.getmembers(conanfile))
    #logging.info("json.dumps(conanfile) = %s", json.dumps(conanfile))
    #pprint.pprint(vars(conanfile))
    #pprint.pprint(vars(conanfile.info))

    logging.info("conanfile.pakcage_id() = %s", conanfile.package_id())
    logging.info("conanfile.info.pakcage_id() = %s", conanfile.info.package_id())
    logging.info("conanfile.info = %s", conanfile.info)
    package_path = os.path.join(cache.packages(ref), conanfile.info.package_id())
    logging.info("package_path = %s", package_path)
    BinaryInstaller._call_package_info(conanfile, package_path)
    logging.info("conanfile.env = %s", conanfile.env)
    logging.info("conanfile.env_info = %s", conanfile.env_info)
    logging.info("conanfile.env_info.vars = %s", conanfile.env_info.vars)
    '''

    # load_from_package
    #what = levels[len(levels) - 2][0]
    #logging.info("what = %s", what)

    '''
    logging.info("conanfile = %s", conanfile)
    logging.info("conanfile.settings.values = %s", conanfile.settings.values)
    logging.info("conanfile.options.values = %s", conanfile.options.values)
    logging.info("conanfile.env = %s", conanfile.env)
    logging.info("conanfile.env.items() = %s", conanfile.env.items())


    conanfile = [node for node in deps_graph.by_levels()[0]][0].conanfile
    logging.info("conanfile = %s", conanfile)
    logging.info("conanfile.env.items() = %s", conanfile.env.items())
    logging.info("conanfile.env_info = %s", conanfile.env_info)
    deets = api.inspect(refs, False)
    logging.info("deets = %s", deets)
    path = api.get_path(refs)
    logging.info("path = %s", path)
    '''
